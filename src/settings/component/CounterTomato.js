import React from 'react';

class CounterTomato extends React.Component {
  render() {
    const { value } = this.props;
    return (
      <div>
        <span
          className="counter-minus"
          onClick={this.onMinus.bind(this)}
        >
          <i>minus</i>
        </span>
        <span>{value / 60}</span>
        <span 
          className="counter-plus"
          onClick={this.onPlus.bind(this)}
        >
          <i>plus</i>
        </span>
      </div>
    );
  }

  onMinus() {
    const { value, name, onChangeSettings } = this.props;
    const min = value / 60; 

    if(min === 0) {
      return;
    }
    
    onChangeSettings( name, (min - 1)*60 );
  }

  onPlus() {
    const { value, name, onChangeSettings } = this.props;
    const min = value / 60; 

    if(min > 59) {
      return;
    }

    onChangeSettings( name, (min + 1)*60 );
  }
}

export default CounterTomato;
