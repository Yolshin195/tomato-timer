import React from 'react';
import SettingsBox from './SettingsBox';

class Settings extends React.Component {
  render() {
    const settings = [];
    const { onChangeSettings, tomatoTimer } = this.props;
    for(let key in tomatoTimer) {
      settings.push(<SettingsBox
        name={key}
        value={tomatoTimer[key]}
        checkTime={('work,smallBreak,bigBreak'.includes(key))}
        onChangeSettings={onChangeSettings}
      />);
    }
    console.log('Settings',this.props);
    return (
      <table>
        <thead>
          <tr>
              <th>Name</th>
              <th>Value</th>
          </tr>
        </thead>

        <tbody>
          {settings}
        </tbody>
      </table>
    );
  }
};

export default Settings;
