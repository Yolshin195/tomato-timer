import React from 'react';
import Checkbox from './Checkbox';
import Counter from './Counter';
import CounterTomato from './CounterTomato';

class SettingsBox extends React.Component {
  render() {
    const { name, value, onChangeSettings, checkTime } = this.props;
    var elem = value; 

    if ( typeof value === 'boolean' ) {

    }

    if ( ! isNaN( Number(value) ) ) {
      if(checkTime) {
        elem = <CounterTomato
          onChangeSettings={ onChangeSettings }
          value={value}
          name={name}
        />;
      } else {
        elem = <Counter
          onChangeSettings={ onChangeSettings }
          value={value}
          name={name}
        />;
      }
    } 

    return (
      <tr>
        <td>{name}</td>
        <td>{elem}</td>
      </tr>
    );
  }
};

export default SettingsBox;
