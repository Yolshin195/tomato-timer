import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Settings from './Settings'; 

function onChangeSettings(key, value) {
  console.log('on change settings',key, value);
  return {type: 'CHANGE_SETTING', key, value}
}

const putStateProps = (state) => {
  return {
    tomatoTimer: state.tomatoTimer.settings
  };
}

const putActionsToProps = (dispatch) => {
  return {
    onChangeSettings: bindActionCreators(onChangeSettings, dispatch)
  }
}

export default connect(putStateProps, putActionsToProps)(Settings);
