import React from 'react';
import SettingsBox from './SettingsBox';

class SettingsGroup extends React.Component {

  render() {
    const settings = [];
    const { elem } = this.props;

    for (let key in elem) {
      settings.push(<SettingsBox name={key} value={elem[key]}/>);
    }
    return (
      <div className="z-depth-1"> 
        {settings}
      </div>
    );
  }
}

export default SettingsGroup;
