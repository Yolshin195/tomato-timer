import React from 'react';

class Counter extends React.Component {
  render() {
    const { value } = this.props;
    return (
      <div>
        <span
          className="counter-minus"
          onClick={this.onMinus.bind(this)}
        >
          <i>minus</i>
        </span>
        <span>{value}</span>
        <span 
          className="counter-plus"
          onClick={this.onPlus.bind(this)}
        >
          <i>plus</i>
        </span>
      </div>
    );
  }

  onMinus() {
    const { value, name, onChangeSettings } = this.props;

    if(value === 0) {
      return;
    }
    
    onChangeSettings( name, (value - 1) );
  }

  onPlus() {
    const { value, name, onChangeSettings } = this.props;

    if(value > 59) {
      return;
    }

    onChangeSettings( name, (value + 1) );
  }
}

export default Counter;
