import ReactDOM from 'react-dom';
import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import { TomatoTimer } from './tomatoTimer';
import MainComponent from './component/mainComponent';
import rootReducer from './store/reducers';
import { trackStorForLocalStore, getStateOfLocalStore } from './lib/stateForLocalStore';
import { initialize } from './store/actions';

export const store = createStore( rootReducer ,
  getStateOfLocalStore() );

store.dispatch(initialize());
trackStorForLocalStore(store);

const timer = new TomatoTimer(store);

ReactDOM.render(
  <Provider store={store}>
    <MainComponent/>
  </Provider>,
  document.getElementById('root')
);
