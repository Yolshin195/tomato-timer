import { combineReducers } from 'redux'
import todo from './toDo.reducers.js';
import { reducersTomatoTimer as tomatoTimer } from '../tomatoTimer';
import navbar from './navbar.reducers';

const rootReducer = combineReducers({
  tomatoTimer,
  todo,
  navbar
})

export default rootReducer;
