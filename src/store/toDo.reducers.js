import { combineReducers } from 'redux'
import { 
  ADD_TODO,
  ADD_TOMATO_TODO,
  TOGGLE_TODO,
  SET_VISIBILITY_FILTER_TODO,
  SET_CURRENT_TODO,
  REMOVE_TODO,
  VisibilityFilters,
} from './toDo.actions';

import { PASS_WORK_TOMATO, COMPLITED_WORK_TOMATO } from '../tomatoTimer';

import { INITIALIZE } from './actions';

const { SHOW_ALL } = VisibilityFilters
const initState = {
  currentTask: 0,
  todos: [
    {
      title: 'Подготовить презентацию',
      text: '1) составить план выступления 2) подготовить текст 3) вынести опорные(ключевые) моменты на слайд',
      completed: true,
      lostTomato: 1,
      completedTomato: 3
    },
    {
      title: 'Рассказать про технику помидора',
      text: '1) Кратко об её авторе и истории создания 2) Основные концепции',
      completed: false,
      lostTomato: 0,
      completedTomato: 0
    },
    {
      title: 'Использованные технологии.',
      text: '1) React 2) Redux',
      completed: false,
      lostTomato: 0,
      completedTomato: 0
    }
  ]
}
function visibilityFilter(state = SHOW_ALL, action) {
  switch (action.type) {
    case SET_VISIBILITY_FILTER_TODO:
      return action.filter
    default:
      return state
  }
}

function isCurrentTask(currentTask, todos) {
  if (!todos[currentTask] || todos[currentTask].completed) {
    for (let i = 0; i < todos.length; i++) {
      if ( todos[i].completed === false) {
        return i; 
      }
    }
  }

  return currentTask
}

function todos(state = initState, action) {
  let currentTask = state.currentTask;
  let todos = [];

  switch (action.type) {
    case ADD_TODO:
      todos = [
        ...state.todos,
        {
          title: action.title,
          text: action.text,
          completed: false,
          lostTomato: 0,
          completedTomato: 0
        }
      ];
      currentTask = isCurrentTask(currentTask, todos);
      return{
        ...state,
        todos,
        currentTask
      }
    case PASS_WORK_TOMATO:
      currentTask = isCurrentTask(currentTask, state.todos);
      return {
        ...state, 
        currentTask,
        todos: state.todos.map((todo, index) => {
          if(currentTask === index) {
            return {
              ...todo,
              lostTomato: todo.lostTomato + 1
            }
          }
          return todo;
        }) 
      }
    case COMPLITED_WORK_TOMATO:
      currentTask = isCurrentTask(currentTask, state.todos);
      return {
        ...state, 
        currentTask,
        todos: state.todos.map((todo, index) => {
          if(currentTask === index) {
            return {
              ...todo,
              completedTomato: todo.completedTomato + 1
            }
          }
          return todo;
        }) 
      }
    case ADD_TOMATO_TODO:
      return {
        ...state,
        todos: state.todos.map((todo, index) => {
          if (index === action.index && !todo.completed) {
            var obj = {};
            if(action.tomato) {
              obj.completedTomato = todo.completedTomato + 1; 
            } else {
              obj.lostTomato = todo.lostTomato + 1;
            }
            return Object.assign({}, todo, obj);
          }
          return todo;
        }) 
      }
    case REMOVE_TODO:
        todos = state.todos.filter((todo, index) => 
          index !== action.index
        ) 
        currentTask = isCurrentTask(currentTask, todos); 
      return {
        ...state,
        todos,
        currentTask
      }
    case TOGGLE_TODO:
      todos = state.todos.map((todo, index) => {
        if (index === action.index) {
          return Object.assign({}, todo, {
            completed: !todo.completed
          })
        }
        return todo
      });
      currentTask = isCurrentTask(currentTask, todos);

      return { 
        ...state,
        todos: todos, 
        currentTask
      }
    case SET_CURRENT_TODO:
      return {
        ...state,
        currentTask: action.index
      }
    case INITIALIZE: {
      currentTask = isCurrentTask(currentTask, state.todos);
      return {
        ...state,
        currentTask
      } 
    }
    default:
      return state;
  }
}

const todoReducers = combineReducers({
  visibilityFilter,
  todos
})

export default todoReducers;
