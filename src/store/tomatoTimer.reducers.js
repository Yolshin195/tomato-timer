import { 
  ACTION_TICK_TIMER,
  ACTION_START_TIMER,
  ACTION_NEXT_TIMER,
  ACTION_PAUSE_TIMER,
  TOGGLE_TIMER,
  COUNTER_TOMATO
} from './tomatoTimer.actions.js';

const initialState = {
  remained: 1800,
  timerWork: false,
  currentDuration: 'work',
  timerCounter: 0,
  durationWorkCount: 4,
  durationWork: 1800,
  durationSmallBreak: 300,
  durationBigBreak: 1500,
};

const tomatoTimerReducer = (state = initialState, action) => {
  switch(action.type) {
    case COUNTER_TOMATO:
      return {
        ...state,
        timerCounter: action.counter
      }
    case ACTION_TICK_TIMER:
      return {
        ...state,
        remained: action.payload
      }
    case ACTION_NEXT_TIMER:
      return {
        ...state,
        timerCounter: action.payload,
        timerWork: true
      }
    case TOGGLE_TIMER:
      return {
        ...state,
        timerWork: !state.timerWork
      }
    default:
      return state;
  }
};

export default tomatoTimerReducer;
