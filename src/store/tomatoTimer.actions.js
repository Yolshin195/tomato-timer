import { timer } from '../index';
import startTimer from '../lib/tomatoTimer'

export const ACTION_TICK_TIMER = 'ACTION_TICK_TIMER';
export const ACTION_START_TIMER = 'ACTION_START_TIMER';
export const ACTION_STOP_TIMER = 'ACTION_STOP_TIMER';
export const ACTION_NEXT_TIMER = 'ACTION_NEXT_TIMER';
export const ACTION_PAUSE_TIMER = 'ACTION_PAUSE_TIMER';
export const TOGGLE_TIMER = 'TOGGLE_TIMER';
export const COUNTER_TOMATO = 'COUNTER_TOMATO';

export const stateTomatoTimer = [
  'work',
  'smallBreak',
  'bigBreak'
]

export function initializeTimer(state) {
  const { remained, timerWork } = state.tomatoTimer;

  timer.setDuration(remained);
  if(timerWork) {
    timer.start();
  }
}

export function countTomato(counter) {
  return {type: COUNTER_TOMATO, counter}
}

export function actionToggleTimer(state) {
  timer.toggle(state);

  return {type: TOGGLE_TIMER}
}

export function actionNextTimer() {
  timer.pass();
  return {type: ACTION_NEXT_TIMER}
}

export const actionStopTimer = function() {
  return {
    type: ACTION_STOP_TIMER 
  }
}

export const actionPauseTimer = function() {
  timer.pause();

  return {
    type: ACTION_PAUSE_TIMER,
    payload: null 
  }
};

export const tickTimer = function(t) {
  return {
    type: ACTION_TICK_TIMER,
    payload: t 
  }
}
