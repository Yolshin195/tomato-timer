import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { startTomato, pauseTomato, passTomato } from '../../actions';

class Timer extends React.Component {
  render() {
    //const dispatch = this.props.dispatch;
    const toggle = {};
    const { startTomato, passTomato, pauseTomato,
            min, sec, timerWork } = this.props;
    if(timerWork) {
      toggle.className =  'timer-puse__image table-cll timer_image';
      toggle.onClick = pauseTomato;
    } else {
      toggle.className = 'timer-start__image table-cll timer_image';
      toggle.onClick = startTomato;
    }

    return (
      <div className="timer-container">
        <div className="timer-next table">
          <div className="table-cll" onClick={ passTomato } >
            Next
          </div>
        </div>
        <div className="timer-time table">
          <div className="table-cll">
            <span class="minutes">{min}</span>
            <span class="delemit">:</span>
            <span class="second">{sec}</span>
          </div>
        </div>
        <div className="timer-start table">
          <div 
            className={toggle.className}
            onClick={toggle.onClick}
          >
          </div>
        </div>
      </div>
    );
  }
};

const putStateProps = (state) => {
  return {
    min: state.tomatoTimer.min,
    sec: state.tomatoTimer.sec,
    timerWork: state.tomatoTimer.timerWork
  };
}

const putActionsToProps = (dispatch) => {
  return {
    passTomato: bindActionCreators(passTomato, dispatch),
    startTomato: bindActionCreators(startTomato, dispatch),
    pauseTomato: bindActionCreators(pauseTomato, dispatch)
  }
}

export default connect(putStateProps, putActionsToProps)(Timer);
