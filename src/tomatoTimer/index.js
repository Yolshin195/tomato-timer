import reducers from './reducers';
import { TomatoTimer as TomatoTimerClass } from './tomatoTimer';
import TomatoTimerComponent from './component/Timer';

export const COMPLITED_WORK_TOMATO = 'COMPLITED_WORK_TOMATO';
export const PASS_WORK_TOMATO = 'PASS_WORK_TOMATO';

export const reducersTomatoTimer = reducers;
export const TomatoTimer = TomatoTimerClass;
export default TomatoTimerComponent;
