import Timer from '../lib/time';
import timeToString from '../lib/helpers/time';
import { 
  nextTomato,
  actionTikTimer,
  compliteWorkTomato,
  passWorkTomato,
  startTomato
} from './actions';


export const WORK = 'work';
export const START = 'start';
export const NEXT = 'next'; 
export const PASS = 'pass'; 
export const PAUSE = 'pause';

export const SMALL_BREAK = 'smallBreak';
export const BIG_BREAK = 'bigBreak';

export class TomatoTimer {
  constructor(store) {
    let { stateTomato, stateTimer, timerWork, remained }  = store.getState().tomatoTimer;

    this.timer = new Timer();
    this.stateTimer = stateTimer || null;
    this.stateTomato = stateTomato || null;
    this.store = store;
    this.storeSubscribe = null;
    this.initTimer();
    this.initStore();

    if(timerWork) {
      this.timer.setDuration( remained );
      this.timer.start();
    }
  }

  initTimer() {
    this.timer.on('tick', (remained) => {
      let { sec, min } = timeToString(remained);

      this.store.dispatch( actionTikTimer(remained, sec, min) );  
    });

    this.timer.on('stop', () => {
      let state = this.store.getState();

      if ( state.tomatoTimer.stateTomato === WORK ) {
        this.store.dispatch( compliteWorkTomato() );
      }
      
      this.store.dispatch( nextTomato() );
    })
  }

  initStore() {
    if ( this.storeSubscribe !== null ) {
      return;
    }
    this.store.subscribe(() => { 
      const { tomatoTimer } = this.store.getState();
      const { stateTomato, stateTimer,
        settings, remained } = tomatoTimer;

      if (this.stateTimer === stateTimer
      && this.stateTomato === stateTomato ) {
        return;
      }

      const letStateTomato = this.stateTomato;
      this.stateTimer = stateTimer;
      this.stateTomato = stateTomato;

      switch (stateTimer) {
        case START: {
          this.timer.setDuration(
            remained || settings[stateTomato]);
          this.timer.start();
          break;
        }
        case NEXT: {
          this.nextTimer(
            settings[stateTomato]); 
          break;
        }
        case PAUSE: {
          this.timer.pause();
          break;
        }
        case PASS: {
          this.nextTimer(
            settings[stateTomato]); 

          if (letStateTomato === WORK) {
            this.store.dispatch( passWorkTomato() ); 
          }
          return;
        }
      }
    });
  }

  nextTimer(time) {
    this.timer.stop(true);
    if(time)
      this.timer.setDuration(time);
    this.timer.start();
  }
}

export function getNextStateOfTomato(timerCounter, durationWorkCount) {
  if(timerCounter === durationWorkCount*2) {
    return BIG_BREAK;
  }

  if(!(timerCounter % 2)) {
    return SMALL_BREAK;
  }

  return WORK;
}
