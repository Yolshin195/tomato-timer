import {COMPLITED_WORK_TOMATO, PASS_WORK_TOMATO} from './index';

export const CHANGE_STATE_TIMER = 'CHANGE_STATE_TIMER';
export function actionNextTimer(stateTimer) {
  return {type: CHANGE_STATE_TIMER, stateTimer }; 
}

export const TIK_TIMER = 'TIK_TIMER';
export function actionTikTimer(remained, sec, min) {
  return {
    type: TIK_TIMER,
    remained,
    sec,
    min
  }
}

export function compliteWorkTomato() {
  console.log('complite');
  return { type: COMPLITED_WORK_TOMATO }
}

export function passWorkTomato() {
  console.log('pass');
  return { type: PASS_WORK_TOMATO }
}

export const START_TOMATO = 'START_TOMATO';
export function startTomato() {
  return { type: START_TOMATO, payload: true }
}

export const PAUSE_TOMATO = 'PAUSE_TOMATO';
export function pauseTomato() {
  return { type: PAUSE_TOMATO, payload: false }
}

export const NEXT_TOMATO = 'NEXT_TOMATO';
export function nextTomato() {
  return { type: NEXT_TOMATO }
}

export const PASS_TOMATO = 'PASS_TOMATO';
export function passTomato() {
  return { type: PASS_TOMATO }
}
