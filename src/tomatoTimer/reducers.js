import {
  START_TOMATO,
  PAUSE_TOMATO,
  NEXT_TOMATO,
  TIK_TIMER,
  PASS_TOMATO
} from './actions';
 
import { 
  getNextStateOfTomato,
  WORK,
  START,
  NEXT,
  PASS,
  PAUSE 
} from './tomatoTimer';

const settings = {
  work: 1800,
  smallBreak: 600,
  bigBreak: 1500,
  counterWorkTomato: 4,
  tomatoDay: 12
}

const initState = {
  remained: null,
  min: '00',
  sec: '00',
  timerWork: false,
  stateTimer: 'stop', 
  stateTomato: 'work',
  counterTimer: 1,
  counterTimerAll: 1,
  settings
}
const CHANGE_SETTING = 'CHANGE_SETTING';

function settingsReducers(state=settings, action) {
  switch(action.type) {
    case CHANGE_SETTING: {
      if( state[action.key] !== undefined ) {
        return {
          ...state,
          [action.key]: action.value
        }
      }
      return state;
    }
    default:
      return state;
  } 
}

function reducers(state=initState, action) {
  var settings = settingsReducers(state.settings, action);

  switch(action.type) {
    case TIK_TIMER: 
      return {
        ...state,
        min: action.min,
        sec: action.sec,
        remained: action.remained
      }
    case START_TOMATO:
      return {
        ...state,
        timerWork: true,
        stateTimer: START
      }
    case PAUSE_TOMATO:
      return {
        ...state,
        timerWork: false,
        stateTimer: PAUSE 
      }
    case NEXT_TOMATO: {
      let counterTimer = state.counterTimer + 1;
      let counterTimerAll = counterTimer;
      let stateTomato =  getNextStateOfTomato(
        counterTimer, 
        settings.counterWorkTomato
      );

      if(counterTimer === state.settings.counterWorkTomato * 2) {
        counterTimer = 0;
      }

      return {
        ...state,
        timerWork: true,
        stateTomato,
        stateTimer: NEXT,
        counterTimer,
        counterTimerAll
      }
    }
    case PASS_TOMATO: {
      let counterTimer = state.counterTimer + 1;
      let counterTimerAll = counterTimer;
      let stateTomato =  getNextStateOfTomato(
        counterTimer, 
        state.settings.counterWorkTomato
      );

      if(counterTimer === state.settings.counterWorkTomato * 2) {
        counterTimer = 0;
      }

      return {
        ...state,
        timerWork: true,
        stateTomato,
        stateTimer: PASS,
        counterTimer,
        counterTimerAll
      }
    }
    default:
      return {
        ...state,
        settings
      };
  }
}

export default reducers;
