import Timer from './time';
//const Timer = require('./time');

class TomatoTimer extends Timer {
  /*
   * @param {object store} store
   */
  constructor(state) {
    super();
    this.timerCounter = 0,
    this.setting = {
      continueWork: true,
      durationWorkCount: 4,
      durationWork: 1800,
      durationSmallBreak: 300,
      durationBigBreak: 1500
    }


    if(typeof state === 'object') {
      this.initState(state)
    }

    this.on('stop', () => {
      if(this.setting.continueWork) {
        this.start();
      }
    })
  }

  initState(state) {
    this.setSetting(state);
    if(state.timerCounter) {
      this.timerCounter = state.timerCounter;
    }

    if(state.remained) {
      this.setDuration(state.remained);
    }
    
    if(state.timerWork) {
      this.start();
    }
  }

  start() {
    if(this.timer) {
      return;
    }

    if(!this.isTimer()) {
      this.timerCounter += 1;
      this.nextTimer(this.setting);
    }

    var time = this.remained ? this.remained : this.duration * 1000;

    this.startTime = Date.now();
    this.endTime = this.startTime + time;

    this.timer = setInterval(this.tick.bind(this), 138);

    this.emit('start', this.timerCounter);
  }

  pass() {
    this.stop(); 
    this.start();

    this.emit('pass', (!this.isTimer()));
  }
  
  /*
   * @param {boolean} state;
   */
  toggle(state) {
    let work = this.isTimer();

    if(work === state) {
      return;
    }

    if(state) {
      this.start();
    } else {
      this.pause();
    }
  }

  nextTimer({durationWorkCount, durationBigBreak, durationSmallBreak, durationWork}) {
    console.log('tomatoTimer - ', this);
    if(this.timerCounter === durationWorkCount*2) {
      this.setDuration(durationBigBreak);
      return this.emit('state', 'bigBreak');
    }

    if(!(this.timerCounter % 2)) {
      this.setDuration(durationSmallBreak);
      return this.emit('state', 'smallBreak');
    }

    this.setDuration(durationWork);
    return this.emit('state', 'work');

  }

  setSetting(settings) {
    if(typeof settings !== 'object') {
      return;
    }

    for(let key in this.setting){
      if(settings[key] === undefined) {
        continue;
      }
      this.setting[key] = settings[key];
    }
  }
}

export default TomatoTimer;

//const timer = new TomatoTimer();
//
//timer.on('tick', function(mesage){
//  console.log('tick', mesage);
//});
//
//timer.on('start', function(mesage){
//  console.log('start',mesage);
//});
//
//timer.on('stop', function(mesage){
//  console.log('stop', mesage);
//});
//
//timer.on('pause', function(mesage){
//  console.log('pause', mesage);
//});
//
//timer.on('state', function(mesage) {
//  console.log('state', mesage);
//});
//
//timer.on('pass', function(mesage) {
//  console.log('pass', mesage);
//});
