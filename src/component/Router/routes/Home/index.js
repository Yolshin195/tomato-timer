import React from 'react';

import Timer from '../../../../tomatoTimer';
import Navbar from '../../../Navbar';

const RouterHome = () => {
  return(
    <div className="row">
      <div className="col s12">
        <Timer/>
      </div>
    </div>
  )
}

export default RouterHome;
