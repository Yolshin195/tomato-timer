import React from 'react';

import Timer from '../../../../tomatoTimer';
import Settings from '../../../../settings';

const RouterSettings = () => {
  return(
    <div className="row">
      <div className="col s8">
        <Timer/>
      </div>
      <div className="col s4">
        <Settings/>
      </div>
    </div>
  )
}

export default RouterSettings;
