import React from 'react';

import FilterTodo from '../../../Todo/Filter';

const RouterFilter = () => {
  return(
    <div className="row">
      <div className="col s12">
        <FilterTodo/>
      </div>
    </div>
  )
}

export default RouterFilter;
