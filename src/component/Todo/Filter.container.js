import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setVisibilityFilter } from '../../store/toDo.actions'

import Filter from './Filter';

const putStateToProps = (state) => {
  return {
      selectedFilter: state.todo.visibilityFilter
  }
}

const putActionsToProps = (dispatch) => {
  return {
    onSetVisibilityFilter: bindActionCreators(setVisibilityFilter, dispatch),
  }
}

export default connect(putStateToProps, putActionsToProps)(Filter);
