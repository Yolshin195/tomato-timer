import React from 'react';
import { connect } from 'react-redux';

class TodoFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                "SHOW_ALL":"Все",
                "SHOW_ACTIVE":"Активные",
                "SHOW_COMPLETED":"Завершонные"
            }
        }
    }

    renderOptions() {
        var arr = [];

        for(var key in this.state.options) {
            if(this.props.selectedFilter === key) {
                arr.push(<option value={key} selected="selected">{this.state.options[key]}</option>);
            } else {
                arr.push(<option value={key}>{this.state.options[key]}</option>);
            }
        }

        return arr;
    }

    setFilter(event) {
        this.props.onSetVisibilityFilter(event.target.value);
    }

    render() {
        return(
            <select style={{display: "inline-block"}} onChange={this.setFilter.bind(this)}>
              {this.renderOptions()}
            </select>
        );
    }
}

export default TodoFilter;
