import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import time from '../../lib/helpers/time';
import { actionToggleTimer, actionNextTimer } from '../../store/tomatoTimer.actions';

class Timer extends React.Component {
  render() {
    //const dispatch = this.props.dispatch;
    const {remained, timerWork, toggleTimer, nextTimer} = this.props;
    const { min, sec } = time(remained);
    return (
      <div className="timer-container">
        <div className="timer-next table">
          <div
            className="table-cll"
            onClick={nextTimer}
          >
            Next
          </div>
        </div>
        <div className="timer-time table">
          <div className="table-cll">
            <span class="minutes">{min}</span>
            <span class="delemit">:</span>
            <span class="second">{sec}</span>
          </div>
        </div>
        <div className="timer-start table">
          <div 
            className={timerWork ? 'timer-puse__image table-cll timer_image':  'timer-start__image table-cll timer_image'}
            onClick={() => {toggleTimer(!timerWork)}}
          >
          </div>
        </div>
      </div>
    );
  }
};

const putStateProps = (state) => {
  return {
    remained: state.tomatoTimer.remained,
    timerWork: state.tomatoTimer.timerWork
  };
}

const putActionsToProps = (dispatch) => {
  return {
    nextTimer: bindActionCreators(actionNextTimer, dispatch),
    toggleTimer: bindActionCreators(actionToggleTimer, dispatch)
  }
}

export default connect(putStateProps, putActionsToProps)(Timer);
